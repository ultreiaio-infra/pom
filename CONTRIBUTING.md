# Install commands

See [Ultreiaio-infra/scripts](https://gitlab.com/ultreiaio-infra/scripts/blob/master/README.md).

# Release

```
ultreiaio-release-start
ultreiaio-release-finish
ultreiaio-stage-close-and-release
```
