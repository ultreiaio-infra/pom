[![Maven Central status](https://img.shields.io/maven-central/v/io.ultreia.maven/pom.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22io.ultreia.maven%22%20AND%20a%3A%22pom%22)
[![build status](https://gitlab.com/ultreiaio/pom/badges/develop/pipeline.svg)](https://gitlab.com/ultreiaio/pom/commits/develop)
[![The GNU Lesser General Public License, Version 3.0](https://img.shields.io/badge/license-LGPL3-grren.svg)](http://www.gnu.org/licenses/lgpl-3.0.txt)
# Abstract

## Requirements

* Use gitlab.com project
* Use *git*
* Use *gitflow*

## What can I do for you, sir?

* infrastructure configuration
* maven plugins versions
* release configuration
* some useful configuration in profiles

## No I don't do this!

* dependencies version
* scm configuration (?) 
* site deployment at release release

# Use pom

## Mandatory configuration

* Mandatory property **organizationId** project's gitlab organization name
* Mandatory property **projectId** project's gitlab name

## To build javadoc
  Add in maven pom:
  
```
<maven.javadoc.skip>false</maven.javadoc.skip>
```

## To build sources
  Add in maven pom:

```
<maven.source.skip>false</maven.source.skip>
```

# Do a release

Use the gitlab job **release**.

### What is done?

* deploy artifacts on nexus repository
* create the git merge to master
* create the git release tag

### What is not done?

* generate the maven site
* deploy the maven site

# Deploy a maven site

## Using Maven

  Go where you need to be on git, and launch

```
mvn clean verify site-deploy -DperformRelease
```

###  Where is deployed the site ?

  If the project is in *Snapshot* version, the maven site is deployed to *develop* version, otherwise it is deployed to the project version.

# How to write a site

*Pom* offers you a minimal site descriptor with common configuration, you just need to add:

* bannerLeft
* bannerRight
* links
* breadcrumbs
* menu items
* footer

## Site urls

* The site top url is **${project.url}**. (example http://jaxx.nuiton.org).
* A version url is **${project.url}/v/${siteDeployClassifier}** (example http://jaxx.nuiton.org/v/2.8).

*SiteDeployClassifier* represents the version of your project or *develop* if the version is a snapshot.

You don't have to figure out how to configure this property, since version 8 it is injected for you.

## Breadcrumbs

On the root module site descriptor, you need to add two breadcrumbs:
```
    <breadcrumbs>
      <item name="${project.name}" href="${project.url}/index.html"/>
      <item name="${project.version}" href="${project.url}/v/${siteDeployClassifier}/index.html"/>
    </breadcrumbs>
```

But on a child module, only one:
```
    <item name="${project.name}" href="index.html"/>
```
## Language

In your main pom, configure it by adding the *locales* property:
```
  <locales>fr</locales>
```

In each site descriptor, use the correct language in the footer *locale* attribute.

## Using velocity extension in your sources

Any site file can be suffixed by *.vm*, a velocity processing will be done before generating the render.

Note that any such file should be added in the *scmwebeditor_vmFiles* attribute in the footer.

## Forbid edition of some files in scmwebeditor

If you don't want to edit a file in scmwebeditor (says to hide the edit link in the develop site), then you must fill the *scmwebeditor_skipFiles* attribute in your footer.

## Footer

In the footer we define a special tag with id **mavenProjectProperties** to inject some meta of your project used later by a javascript file to render the site.

Example:

```
        <div id='mavenProjectProperties' 
             locale='fr'
             projectId='${project.projectId}'
             version='${siteDeployClassifier}'
             sourcesType='${project.siteSourcesType}'
             scmwebeditor_vmFiles=",update,install,"
             scmwebeditor_skipFiles=",index,"/>
```

* *locale* is the site locale (fr, en, ...)
* *projectId* is the project's id
* *version* is the site project's version (if snapshot then *develop* is used)
* *sourcesType* is the extension of format you are using in your project (default is *apt*, but could be *rst*, *md*, ...)
* *scmwebeditor_vmFiles* is a list of your files using velocity, this is meaning that files are suffixed by *.vm*.
* *scmwebeditor_skipFiles* is a list of your read-only files to not edit in scmwebeditor (mainly report files). 

Please note the formatting of the list in *scmwebeditor_vmFiles* and *scmwebeditor_skipFiles*.

## Example

Best example is a live example, See https://gitlab.nuiton.org/nuiton/i18n/tree/develop/src/site.

# FAQ

## How can I change the java level on my project ?

You need to overwrite three properties :

* javaVersion
* signatureArtifactId
* signatureVersion

**javaVersion** define the java level at compile time

**signatureArtifactId** is the animal-sniffer signature artifact artifactId used to check your code is compliant with the java level chosen

**signatureVersion** is the animal-sniffer signature artifact version used to check your code is compliant with the java level chosen

Since pom 9, those properties are mandatory, See https://gitlab.nuiton.org/nuiton/pom/blob/develop/CHANGELOG.md#to-version-9x

## How can I make my jar executable ?

One way is to get all dependencies needed and tune the jar manifest.

Here is an example of that:

```
      <plugin>
        <artifactId>maven-dependency-plugin</artifactId>
        <executions>
          <execution>
            <id>copy-dependencies</id>
            <goals>
              <goal>copy-dependencies</goal>
            </goals>
            <phase>prepare-package</phase>
            <configuration>
              <overWriteReleases>false</overWriteReleases>
              <overWriteSnapshots>true</overWriteSnapshots>
              <overWriteIfNewer>true</overWriteIfNewer>
              <outputDirectory>${project.build.directory}/lib</outputDirectory>
              <silent>true</silent>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <artifactId>maven-jar-plugin</artifactId>
        <configuration>
          <archive>
            <manifest>
              <useUniqueVersions>false</useUniqueVersions>
              <addClasspath>true</addClasspath>
              <classpathPrefix>./lib/</classpathPrefix>
              <mainClass>fqn.de.la.main.classe</mainClass>
            </manifest>
          </archive>
        </configuration>
      </plugin>
```

Another solution is to use the jar-with-dependencies assembly descriptor.

**TODO example**

## My release was deployed but the git last part fail, how can can I finish my release ?

Just launch this command:

```
mvn jgitflow:release-finish -DnoReleaseBuild
```

## How can I make a release with a snapshot dependency ?

Yes some people does this! This is not a good idea at all, but whatever...

Just add **-DallowSnapshots=true** to the two jgitflow commands:

```
mvn jgitflow:release-start -DallowSnapshots=true
mvn jgitflow:release-finish -DallowSnapshots=true
```

## How can I start a maintenance branch ?

From the last tag, create a *master-x* branch, then from this branch a *develop-x* branch.

On that *develop-x* branch add in the property section this configuration:
```
    <jgitflow.masterBranchName>master-x</jgitflow.masterBranchName>
    <jgitflow.developBranchName>develop-x</jgitflow.developBranchName>
```

**Note:** To start a new feature think always to add the new starting develop branch:
```
git flow feature start myFeature develop.x
```

Release process stay the same, nothing change :).

## How can I shade my application jar ?

Benefit is to get a single application jar.

Short answer: use the Shade Maven plugin.

Get an example in this project (search for **assembly-profile**):

[observe application swing](https://gitlab.com/ultreia.io/ird-observe/blob/develop/application-swing/pom.xml).
