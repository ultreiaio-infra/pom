#!/bin/sh
for f in $(grep -m 1 -I -nr 'org.apache.commons.logging.Log' . | grep -v '.git' | awk -F ':' '{print $1}') ; do \
  echo $f ; \
  sed  -i -e 's/commons.logging.LogFactory;/logging.log4j.LogManager;/g' $f ; \
  sed  -i -e 's/commons.logging.Log/logging.log4j.Logger/g' $f ; \
  sed  -i -e 's/ Log / Logger /g' $f ; \
  sed  -i -e 's/LogFactory.getLog/LogManager.getLogger/g' $f  ; \
done